import React from 'react'
import tepoz from '../assets/tepoz.jpg'
import './ComponenteDos.css'
function ComponenteDos() {
  return (
    <div className='container2'>
      <img src={tepoz} className='imagenTepoz' width="320" height="500"/>
      <div className='personal'>
        <h1>Nombre: Diego Noel Gonzalez Tamariz</h1>
        <h1>Carrera: ISC</h1>
        <h1>Numero de control: 19680159</h1>
        <h1>Asignatura: Servicios web en disp. moviles</h1>
        <h1>Fecha de nacimiento: 09/12/2001</h1>
        <h1>Lugar de nacimiento: Moreloyos</h1>
        <h1>Semestre: 8vo</h1>
      </div>
    </div>
  )
}
export default ComponenteDos