import React from 'react'
import './ComponenteTres.css'
import fb from '../assets/facebook.png'
import ig from '../assets/insta.png'
import tw from '../assets/tw.png'
import yt from '../assets/yt.png'
function ComponeteTres() {
  return (
    <div className='contenedor'>
      <a href="https://www.facebook.com/Gonzlez.diego04/"><img className='imagen' src={fb} width="50" height="50"/></a>
      <a href="https://twitter.com/DiegoGonzaUuU?t=KLln7wG4wmf8T-pn2hwF-w&s=09"><img className='imagen' src={tw} width="50" height="50"/></a>
      <a href="https://youtube.com/@diegonoelgonzaleztamariz2878"><img className='imagen' src={yt} width="50" height="50"/></a>
      <a href="https://instagram.com/_diego_kun_7u7?igshid=ZDdkNTZiNTM="><img className='imagen' src={ig} width="50" height="50"/></a>
    </div>
  )
}
export default ComponeteTres